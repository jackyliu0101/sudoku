# Sudoku App Description
This is a sudoku side project created by Jacky Liu and Favian (Ian) Samatha. 

### How to use:
1. Install all dependencies with
```npm Install ```
2. Run the server with
```node server.js```
3. Enter username as "test" and password as "test"
4. Click 'new game' and choose an option


### Some Screenshots:
- Login Page
![Login Page Screenshot](/img/LoginScreen.png)
- Choose Difficulty Menu
![Choose Difficulty Screenshot](/img/Menu.png)
- Gameplay
![Gameplay Screenshot](/img/Gameplay.png)

## Authors:
[Favian (Ian) Samatha](https://github.com/faviansamatha)
[Jacky Liu](https://github.com/jackyliu0101)